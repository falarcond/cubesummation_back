﻿using CubeSummation.Entities;
using CubeSummation.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CubeSummation.Application
{
    public static class TestCaseConstrains
    {
        public static bool ValidateConstrains(this TestCase testCase)
        {
            bool result;

            bool matrix3dSize = Matrix3dSize(testCase.N);
            bool commandsSize = CommandsSize(testCase.M);
            bool coordinatesOfQuery = CoordinatesOfQuery(testCase);
            bool coordinatesOfUpdate = CoordinatesOfUpdate(testCase);
            bool itemValue = ItemValue(testCase.Commands);

            result = matrix3dSize &&
                coordinatesOfUpdate &&
                commandsSize &&
                coordinatesOfQuery &&
                coordinatesOfUpdate &&
                itemValue;

            return result;
        }

        public static bool ValidateT(byte testCasesLenght)
        {
            return testCasesLenght >= 1 && testCasesLenght <= 50;
        }

        private static bool Matrix3dSize(byte N)
        {
            return N >= 1 && N <= 100;
        }

        private static bool CommandsSize(short M)
        {
            return M >= 1 && M <= 1000;
        }

        private static bool CoordinatesOfQuery(TestCase testCase)
        {
            bool result;

            result = testCase.Commands.Where(cmd => { return cmd.CommandType == CommandTypeEnum.Query; }).All(cmd => {
                bool validX = cmd.Coor1.X >= 0 && cmd.Coor1.X <= cmd.Coor2.X && cmd.Coor2.X <= testCase.N - 1;
                bool validY = cmd.Coor1.Y >= 0 && cmd.Coor1.Y <= cmd.Coor2.Y && cmd.Coor2.Y <= testCase.N - 1;
                bool validZ = cmd.Coor1.Z >= 0 && cmd.Coor1.Z <= cmd.Coor2.Z && cmd.Coor2.Z <= testCase.N - 1;

                return validX && validY && validZ;
            });

            return result;
        }

        private static bool CoordinatesOfUpdate(TestCase testCase)
        {
            bool result;

            result = testCase.Commands.Where(cmd => { return cmd.CommandType == CommandTypeEnum.Update; }).All(cmd => {
                bool validX = cmd.Coor1.X >= 0 && cmd.Coor1.X <= testCase.N - 1;
                bool validY = cmd.Coor1.Y >= 0 && cmd.Coor1.Y <= testCase.N - 1;
                bool validZ = cmd.Coor1.Z >= 0 && cmd.Coor1.Z <= testCase.N - 1;

                return validX && validY && validZ;
            });

            return result;
        }

        private static bool ItemValue(List<Command> commands)
        {
            bool result;

            result = commands.Where(cmd => { return cmd.CommandType == CommandTypeEnum.Update; }).All(cmd => {
                return cmd.W >= (Math.Pow(10, 9) * -1) && cmd.W <= Math.Pow(10, 9);
            });

            return result;
        }
    }
}
