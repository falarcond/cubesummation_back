﻿using CubeSummation.Entities;
using CubeSummation.Entities.Constants;
using CubeSummation.Entities.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CubeSummation.Application
{
    public static class TestCaseHelper
    {
        const char SPLIT_CHAR = (char)32;
        private static readonly Regex REG_T = new Regex(CustomRegex.T);
        private static readonly Regex REG_NM = new Regex(CustomRegex.NM);
        private static readonly Regex REG_UPDATE = new Regex(CustomRegex.UPDATE);
        private static readonly Regex REG_QUERY = new Regex(CustomRegex.QUERY);

        public static TestCase[] TryCastToTestCase(this string[] lines, byte cantTestCases, out bool result)
        {
            result = true;
            TestCase[] testCases = BuildTestCasesFromT(cantTestCases);
            int counterItems = 0;
            int startIndex = 0;

            for (byte i = 0; i < cantTestCases; i++)
            {
                startIndex += (testCases[i == 0 ? 0 : (i - 1)]?.M == null ? 0 : testCases[i - 1].M + 1);

                SetNandM(ref testCases[i], lines[startIndex]);
                string[] commands = new string[testCases[i].M];

                Array.Copy(lines, startIndex + 1, commands, 0, testCases[i].M);
                LoadCommands(ref testCases[i], commands);
                counterItems += testCases[i].Commands.Count + 1;

                if (!testCases[i].ValidateConstrains())
                {
                    result = false;
                    throw new Exception("Format wrong.", new Exception());
                }
            }

            if (lines.Length != counterItems)
            {
                result = false;
                throw new Exception("Format wrong.", new Exception());
            }

            return testCases;
        }

        public static List<long> Result(this TestCase[] testCases)
        {
            List<long> resultSummation = new List<long>();
            Matrix3d matrix3D = new Matrix3d();

            foreach (var testCase in testCases)
            {
                matrix3D.Data = new long[testCase.N, testCase.N, testCase.N];
                foreach (var cmd in testCase.Commands)
                {
                    if (cmd.CommandType == CommandTypeEnum.Update)
                    {
                        matrix3D.UpdateItem(cmd.Coor1, cmd.W ?? 0);
                    }
                    else
                    {
                        long sum = matrix3D.SumRange(cmd.Coor1, cmd.Coor2);
                        resultSummation.Add(sum);
                    }
                }
            }
            return resultSummation;
        }

        public static void SetNandM(ref TestCase testCase, string nmChain)
        {
            if (REG_NM.IsMatch(nmChain))
            {
                string n = nmChain.Split(SPLIT_CHAR)[0];
                string m = nmChain.Split(SPLIT_CHAR)[1];

                testCase = new TestCase
                {
                    N = byte.Parse(n),
                    M = short.Parse(m)
                };
            }
            else
            {
                throw new Exception("Format wrong.", new Exception());
            }
        }

        public static void LoadCommands(ref TestCase testCase, string[] commands)
        {
            if (testCase.M == commands.Length)
            {
                testCase.Commands = new List<Command>();
                foreach (var cmd in commands)
                {
                    string[] itemsCmd = cmd.Split(SPLIT_CHAR);
                    string[] coords = new string[itemsCmd.Length - 1];
                    Array.Copy(itemsCmd, 1, coords, 0, itemsCmd.Length - 1);
                    long[] coordsB = Array.ConvertAll(coords, long.Parse);

                    if (REG_UPDATE.IsMatch(cmd))
                    {
                        testCase.Commands.Add(LoadCmdUpdate(coordsB));
                    }
                    else if (REG_QUERY.IsMatch(cmd))
                    {
                        testCase.Commands.Add(LoadCmdQuery(coordsB));
                    }
                    else
                    {
                        throw new Exception("Format wrong.", new Exception());
                    }
                }
            }
        }

        private static TestCase[] BuildTestCasesFromT(byte tChain)
        {
            TestCase[] result;
            if (REG_T.IsMatch(tChain.ToString()))
            {
                result = new TestCase[tChain];
            }
            else
            {
                throw new Exception("Format wrong.", new Exception());
            }

            return result;
        }

        private static Command LoadCmdUpdate(params long[] coords)
        {
            Command result;
            if (coords.Length == 4)
            {
                result = new Command
                {
                    CommandType = CommandTypeEnum.Update,
                    Coor1 = new Coordinate<byte>
                    {
                        X = (byte)(coords[0] - 1),
                        Y = (byte)(coords[1] - 1),
                        Z = (byte)(coords[2] - 1)
                    },
                    W = coords[3]
                };
            }
            else
            {
                throw new Exception("Format wrong.", new Exception());
            }

            return result;
        }

        private static Command LoadCmdQuery(params long[] coords)
        {
            Command result;
            if (coords.Length == 6)
            {
                result = new Command
                {
                    CommandType = CommandTypeEnum.Query,
                    Coor1 = new Coordinate<byte>
                    {
                        X = (byte)(coords[0] - 1),
                        Y = (byte)(coords[1] - 1),
                        Z = (byte)(coords[2] - 1)
                    },
                    Coor2 = new Coordinate<byte>
                    {
                        X = (byte)(coords[3] - 1),
                        Y = (byte)(coords[4] - 1),
                        Z = (byte)(coords[5] - 1)
                    }
                };
            }
            else
            {
                throw new Exception("Format wrong.", new Exception());
            }

            return result;
        }
    }
}
