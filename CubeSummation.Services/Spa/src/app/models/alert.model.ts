export interface AlertModel {
    'alert-success': boolean;
    'alert-warning': boolean;
    'alert-danger': boolean;
}
