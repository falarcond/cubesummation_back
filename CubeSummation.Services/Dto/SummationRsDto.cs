﻿namespace CubeSummation.Presentation.Dto
{
    public class SummationRsDto
    {
        public byte[] Result { get; set; }
    }
}