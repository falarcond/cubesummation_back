﻿using CubeSummation.Entities.Enums;

namespace CubeSummation.Entities
{
    public class Command
    {
        public CommandTypeEnum CommandType { get; set; }
        public Coordinate<byte> Coor1 { get; set; }
        public Coordinate<byte> Coor2 { get; set; }
        public long? W { get; set; }
    }
}
