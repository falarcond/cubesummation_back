﻿using System;
using System.Net;

namespace CubeSummation.Presentation.Dto
{
    public class ResponseApiDto
    {
        public HttpStatusCode Code { get; set; }
        public string Description { get; set; }
        public object Data { get; set; }
    }
}