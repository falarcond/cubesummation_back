﻿namespace CubeSummation.Entities.Constants
{
    public class CustomRegex
    {
        public static readonly string T = @"^(\d+){1}$";
        public static readonly string NM = @"^(\d+\s){1}(\d+)$";
        public static readonly string UPDATE = @"^UPDATE\s(\d+\s){3}([-]?\d+)$";
        public static readonly string QUERY = @"^QUERY\s(\d+\s){5}(\d+)$";
    }
}
