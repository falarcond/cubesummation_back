﻿using System.ComponentModel.DataAnnotations;

namespace CubeSummation.Presentation.Dto
{
    public class SummationRqDto
    {
        [Required(ErrorMessage = "CantTestCases is required")]
        [Range(1, 50, ErrorMessage = "CantTestCases should be between 1 to 50")]
        public byte CantTestCases { get; set; }

        [Required(ErrorMessage = "TestCases are required")]
        public string[] TestCases { get; set; }
    }
}