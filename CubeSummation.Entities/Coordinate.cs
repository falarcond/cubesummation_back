﻿using System;

namespace CubeSummation.Entities
{
    public class Coordinate<T> where T : struct,
          IComparable,
          IComparable<T>,
          IConvertible,
          IEquatable<T>,
          IFormattable
    {
        public T X { get; set; }
        public T Y { get; set; }
        public T Z { get; set; }
    }
}
