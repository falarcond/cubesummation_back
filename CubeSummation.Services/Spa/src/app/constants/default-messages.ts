export const DEFAULT_MESSAGES = {
    TITLE: 'Cube summation',
    CHAIN: {
        INPUT: 'Cadena de entrada',
        OUTPUT: 'Arreglo de salida'
    },
    EXEC_BUTTON: 'Ejecutar',
    PLACEHOLDER: {
        INPUT: 'Las filas son separadas por saltos de línea...',
        OUTPUT: '[...]'
    },
    ALERT: {
        TITLE: {
            SUCCESS: 'Ejecución exitosa!',
            WARNING: 'Advertencia!',
            DANGER: 'Error!'
        },
        SUBTITLE: {
            SUCCESS: 'La operación ha sido ejecutada satisfactoriamente!',
            DANGER: 'La operación no ha sido ejecutada.'
        }
    }
};
