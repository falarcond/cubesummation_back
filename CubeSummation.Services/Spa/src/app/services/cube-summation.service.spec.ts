import { TestBed } from '@angular/core/testing';

import { CubeSummationService } from './cube-summation.service';

describe('CubeSummationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CubeSummationService = TestBed.get(CubeSummationService);
    expect(service).toBeTruthy();
  });
});
