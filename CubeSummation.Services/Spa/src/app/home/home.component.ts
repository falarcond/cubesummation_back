import { Component } from '@angular/core';
import { DEFAULT_MESSAGES } from '../constants/default-messages';
import { CubeSummationService } from '../services/cube-summation.service';
import { AlertModel } from '../models/alert.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public title = DEFAULT_MESSAGES.TITLE;
  public inputMsj = DEFAULT_MESSAGES.CHAIN.INPUT;
  public outputMsj = DEFAULT_MESSAGES.CHAIN.OUTPUT;
  public execButton = DEFAULT_MESSAGES.EXEC_BUTTON;
  public inputPlaceholder = DEFAULT_MESSAGES.PLACEHOLDER.INPUT;
  public outputPlaceholder = DEFAULT_MESSAGES.PLACEHOLDER.OUTPUT;
  public showAlert = false;
  public result: string;
  public loading = false;
  private bulkEditCommands: string[];
  private alertType: Object;
  private alert: Object;

  constructor(private cubeSummationService: CubeSummationService) {
    this.setAlertType();
    this.loadDefaultValues();
  }

  public getCubeSummation(): void {
    if (!!this.bulkEditCommands.length && this.bulkEditCommands.length > 2) {
      this.loading = true;
      let bulkEditCommandsCloned = JSON.parse(JSON.stringify(this.bulkEditCommands));
      const cantTestCases = bulkEditCommandsCloned.shift();
      this.cubeSummationService.execSummation(+cantTestCases, bulkEditCommandsCloned).subscribe(response => {
        this.result = response['Data'];
        // tslint:disable-next-line: no-use-before-declare
        this.setAlertType(AlertType.success);
        this.loading = false;
      }, () => {
        // tslint:disable-next-line: no-use-before-declare
        this.setAlertType(AlertType.danger);
        this.result = 'Format wrong!';
        this.loading = false;
      });
    } else {
      // tslint:disable-next-line: no-use-before-declare
      this.setAlertType(AlertType.warning, 'Ingrese las variables de entrada en el formato correcto.');
    }
  }

  public getAlertType(): AlertModel {
    return {
      'alert-success': this.alertType['success'],
      'alert-warning': this.alertType['warning'],
      'alert-danger': this.alertType['danger']
    };
  }

  public updateBulkEdit(event: any): void {
    if (!!event.target.value || event.target.value === '') {
      const currentValue = event.target.value;
      this.bulkEditCommands = currentValue.split(/\r?\n/);
      if (this.bulkEditCommands.length === 1 && this.bulkEditCommands[0] === '') {
        this.bulkEditCommands = [];
      }
    }
  }

  public closeAlert(): void {
    this.showAlert = false;
  }

  private setAlertType(alertType?: AlertType, warningMessage?: string) {
    setTimeout(() => {
      this.showAlert = false;
    }, 2000);

    this.showAlert = true;

    this.alertType = {
      success: false,
      warning: false,
      info: false,
      danger: false
    };

    switch (alertType) {
      case 'success':
        this.alertType['success'] = true;
        this.alert['title'] = DEFAULT_MESSAGES.ALERT.TITLE.SUCCESS;
        this.alert['subtitle'] = DEFAULT_MESSAGES.ALERT.SUBTITLE.SUCCESS;
        break;
      case 'warning':
        this.alertType['warning'] = true;
        this.alert['title'] = DEFAULT_MESSAGES.ALERT.TITLE.WARNING;
        this.alert['subtitle'] = warningMessage;
        break;
      case 'danger':
        this.alertType['danger'] = true;
        this.alert['title'] = DEFAULT_MESSAGES.ALERT.TITLE.DANGER;
        this.alert['subtitle'] = DEFAULT_MESSAGES.ALERT.SUBTITLE.DANGER;
        break;
      default:
        this.showAlert = false;
        break;
    }
  }

  private loadDefaultValues() {
    this.bulkEditCommands = [];
    this.alert = {
      title: '',
      subtitle: ''
    };
  }
}

enum AlertType {
  success = 'success',
  warning = 'warning',
  danger = 'danger'
}
