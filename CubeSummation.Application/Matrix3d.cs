﻿using CubeSummation.Entities;
using System;

namespace CubeSummation.Application
{
    public class Matrix3d
    {
        public long[,,] Data { get; set; }

        public bool UpdateItem(Coordinate<byte> coordinate, long value) {
            bool result = true;

            try
            {
                Data[coordinate.X, coordinate.Y, coordinate.Z] = value;
            }
            catch (Exception)
            {
                result = false;
                throw;
            }

            return result;
        }

        public long SumRange(Coordinate<byte> coor1, Coordinate<byte> coor2) {
            long result = 0;

            for (int z = 0; z < (coor2.Z - coor1.Z + 1); z++)
            {
                for (int y = 0; y < (coor2.Y - coor1.Y + 1); y++)
                {
                    for (int x = 0; x < (coor2.X - coor1.X + 1); x++)
                    {
                        result += Data[coor1.X + x, coor1.Y + y, coor1.Z + z];
                    }
                }
            }

            return result;
        }
    }
}
