#!/bin/bash

cd ./Spa
npm i
rm -rf dist/cubesummation
rm -f ../*.js
rm -f ../*.js.map
rm -f ../*.ico
rm -f ../*.html
rm -f ../*.txt
rm -f ../*.css

ng build --prod
cp ./dist/cubesummation/*.* ../
