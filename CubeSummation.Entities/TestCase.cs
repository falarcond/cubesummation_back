﻿using System.Collections.Generic;

namespace CubeSummation.Entities
{
    public class TestCase
    {
        public byte N { get; set; }
        public short M { get; set; }
        public List<Command> Commands { get; set; }
    }
}
