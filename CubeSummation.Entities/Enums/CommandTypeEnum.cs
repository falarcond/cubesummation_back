﻿namespace CubeSummation.Entities.Enums
{
    public enum CommandTypeEnum
    {
        Update,
        Query
    }
}
