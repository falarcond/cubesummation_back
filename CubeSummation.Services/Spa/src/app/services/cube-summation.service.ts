import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CubeSummationService {
  private readonly URL_CUBE_SUMMATION_API: string;

  constructor(private http: HttpClient) {
    this.URL_CUBE_SUMMATION_API = '/api/summation/';
  }

  public execSummation(cantTestCases: number, chainCommands: string[]): Observable<number[]> {
    return this.http.post<number[]>(this.URL_CUBE_SUMMATION_API, {
      CantTestCases: cantTestCases,
      TestCases: chainCommands
    }).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}
