﻿using CubeSummation.Application;
using CubeSummation.Entities;
using CubeSummation.Presentation.Dto;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace CubeSummation.Presentation.Controllers
{
    public class SummationController : ApiController
    {
        // GET: api/Summation
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Summation/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Summation
        [HttpPost]
        public ResponseApiDto Post([FromBody]SummationRqDto value)
        {
            ResponseApiDto response = new ResponseApiDto
            {
                Code = HttpStatusCode.BadRequest
            };
            byte cantTestCases = value.CantTestCases;
            if (TestCaseConstrains.ValidateT(cantTestCases))
            {
                TestCase[] testCases = value.TestCases.TryCastToTestCase(cantTestCases, out bool castSuccess);
                if (castSuccess)
                {
                    List<long> summation = testCases.Result();
                    response.Code = HttpStatusCode.OK;
                    response.Data = summation;
                }
            }
            else
            {
                throw new Exception("Format wrong.", new Exception());
            }
            return response;
        }

        // PUT: api/Summation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Summation/5
        public void Delete(int id)
        {
        }
    }
}
